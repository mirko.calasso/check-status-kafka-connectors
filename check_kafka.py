import requests
import json

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


kafka_containers_file = open('./kafka_container.json')
kafka_containers = json.load(kafka_containers_file)
for item in kafka_containers:
    print (bcolors.WARNING + item)
    for kafka_container in kafka_containers[item]:
        print (bcolors.OKCYAN + kafka_container)
        try:
            response = requests.get(kafka_container+'/connectors/')
            print(bcolors.OKBLUE +  response.text)
            connectors = json.loads(response.text)
            for connector in connectors:
                #print(connector)
                status_connector = kafka_container+'/connectors/'+connector+'/status'
                #  print(status_url)
                resp = requests.get(status_connector)
                status = json.loads(resp.text)
                print(bcolors.OKGREEN + connector+' connector: '+ status['connector']['state'])
                print(bcolors.OKGREEN + connector+' task: '+status['tasks'][0]['state'])
                if  status['connector']['state'] != 'RUNNING':
                    print(bcolors.FAIL + 'NOT RUNNIG')
                    requests.post( kafka_container+'/connectors/'+connector+'/restart')
                    print( kafka_container+'/connectors/'+connector+'/restart')
                if status['tasks'][0]['state'] != 'RUNNING':
                    print(bcolors.FAIL + 'NOT RUNNIG')
                    requests.post( kafka_container+'/connectors/'+connector+'/tasks/0/restart')
                    print( kafka_container+'/connectors/'+connector+'/tasks/0/restart')

                #print( kafka_container+'/connectors/'+connector+'/restart')
                #print( kafka_container+'/connectors/'+connector+'/tasks/0/restart')
    #    except:
        except Exception as e:
            print(bcolors.FAIL + kafka_container+' not reachable')
            pass
